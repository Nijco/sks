package org.acme;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("/hello")
public class PromisingControllerClass {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "Welp, please work this time";
    }
}
